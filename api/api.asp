<!--#include file="../inc/AspCms_SettingClass.asp" -->
<!--#include file="json.asp"-->
<%
response.codepage=65001
dim token,ltoken

token ="abcdefg" '通讯码设置

ltoken = request.servervariables("HTTP_TOKEN")
if ltoken <> token then
	'echo "Link Error code"
'	response.end
end if



dim action : action=getForm("action","get")
dim sql,rs,i,json_rows,json_ret,arr_rows,jsonObj,e,varTypm,jsonStr,varTyp,total,totalnum
dim sperStrs,sperStr,spec
Dim myArray()

if action = "contentList" then '通用内容列表
	dim lsort,lnum,lpage,lorder,lsortby,orderStr
	lsort = getForm("sort","get")
	lnum=getForm("num","get")
	lpage=getForm("page","get")
	lorder=getForm("order","get")
	lsortby = getForm("sortby","get")
	
	
	if isNul(lnum) then lnum = 10  else lnum = cint(lnum)
	if isNul(lpage) then lpage = 1  else lpage = cint(lpage)
	if isNul(lorder) then lorder = "ContentID"
	if isNul(lsortby) then lsortby = "desc"
	'echo lsortby
	
	select case lorder
		case "time" : lorder ="a.AddTime" 
	end select
	
	orderStr = " order by " & lorder & " " & lsortby
	
	sperStrs =conn.Exec("select SpecCategory+'_'+SpecField from {prefix}SpecSet Order by SpecOrder Asc,SpecID", "arr")				
	if isarray(sperStrs) then
		sperStr=""
		for each spec in sperStrs
			sperStr = sperStr&","&spec
		next
	end if
	
	'sql="select top "&lnum&" * from {prefix}Content where LanguageID="&setting.languageID&" and ContentStatus=1 and TimeStatus=0 and SortID in ("&getSubSort(lsort, 1)&") ORDER BY ContentID DESC"
	sql="select ContentID,a.SortID,a.GroupID as aGroupID,a.Exclusive,Title,Title2,TitleColor,IsOutLink,OutLink,Author,ContentSource,ContentTag,Content,ContentStatus,IsTop,Isrecommend,IsImageNews,IsHeadline,IsFeatured,ContentOrder,IsGenerated,Visits,a.AddTime,a.[ImagePath],a.IndexImage,a.DownURL,a.PageFileName,a.PageDesc,SortType,SortURL,SortFolder,SortFileName,SortName,ContentFolder,ContentFileName,b.GroupID as bGroupID,b.Exclusive,b.GroupID "&sperStr&" from {prefix}Content as a,{prefix}Sort as b where a.LanguageID="&setting.languageID&" and a.SortID=b.SortID and ContentStatus=1 and TimeStatus=0 and a.SortID in ("&getSubSort(lsort, 1)&")"&orderStr
	
	set rs=conn.Exec(sql,"r1")
	
	if lpage*lnum>rs.recordcount then
		if lpage*lnum - rs.recordcount < lnum then
			totalnum = rs.recordcount
			Redim myArray(rs.recordcount mod lnum -1)
		end if
	else
		totalnum = lnum * lpage
		Redim myArray(lnum-1)
	end if
	
	Set jsonObj=New json
	jsonObj.toResponse=False
	Set json_ret = server.createobject("scripting.dictionary")
	
	dim m
	if (NOT rs.EOF) then rs.Move((lpage-1) * lnum)
	For i=(lpage-1)*lnum To totalnum-1
		m = i - (lpage-1)*lnum
		Set myArray(m) = server.createobject("scripting.dictionary")
		
		For Each e In rs.Fields
			'rows = rows &""""& e.Name & """:""" & replace(e.value,chr(34),"@_'_@") & ""","
			myArray(m).Add e.Name,e.value  '将key/value加到行数组对象中
		Next 
		Rs.movenext
	Next
	total = rs.recordcount
	
elseif action = "navlist" then '栏目列表
	dim vtype
	vtype = getForm("type","get")
	if isnul(vtype) then vtype=0
	
	sql = "select SortName,SortType,SortURL,sortID,(select count (*) from {prefix}Sort as a where a.ParentID=b.sortID) as subcount,SortFolder,SortFileName,GroupID,Exclusive,indeximage,SortEnName,IcoImage,PageDesc,SortContent from {prefix}Sort as b  where LanguageID="&setting.languageID&" and SortStatus=1 and ParentID="&vtype&" order by SortOrder asc"
	
	set rs=conn.Exec(sql,"r1")
	Set jsonObj=New json
	jsonObj.toResponse=False
	Set json_ret = server.createobject("scripting.dictionary")
	
	Redim myArray(rs.recordcount - 1)
	
	
	For i=0 To rs.recordcount-1
		Set myArray(i) = server.createobject("scripting.dictionary")
		
		For Each e In rs.Fields
			'rows = rows &""""& e.Name & """:""" & replace(e.value,chr(34),"@_'_@") & ""","
			myArray(i).Add e.Name,e.value  '将key/value加到行数组对象中
		Next 
		Rs.movenext
	Next
	
	total = rs.recordcount
	
elseif action = "content" then '内容详情
	dim ContentID
	
	ContentID=getForm("contentid","get")
		
	if isarray(sperStrs) then
		for each spec in sperStrs
			sperStr=sperStr&","&spec
		next
	end if
	
	sql = "select ContentID,a.SortID,a.GroupID as aGroupID ,a.Exclusive as aExclusive,b.GroupID as bGroupID,b.Exclusive as bExclusive,Title,Title2,TitleColor,IsOutLink,OutLink,Author,ContentSource,ContentTag,Content,ContentStatus,IsTop,Isrecommend,IsImageNews,IsHeadline,IsFeatured,ContentOrder,IsGenerated,Visits,a.AddTime,a.[ImagePath],a.IndexImage,a.DownURL,a.PageFileName,a.PageDesc,a.PageKeywords,SortType,SortURL,SortFolder,SortFileName,SortName,ContentFolder,ContentFileName,SortTemplate,ParentID,TopSortID,ContentTemplate,IsNoComment "&sperStr&",a.DownGroupID,a.VideoGroupID from {prefix}Content as a,{prefix}Sort as b where a.LanguageID="&setting.languageID&"and a.SortID=b.SortID and ContentStatus=1 and TimeStatus=0 and IsOutLink=0 and ContentID="&contentid
	
	set rs=conn.Exec(sql,"r1")
	Set jsonObj=New json
	jsonObj.toResponse=False
	Set json_ret = server.createobject("scripting.dictionary")
	
	Redim myArray(rs.recordcount - 1)
	
	
	For i=0 To rs.recordcount-1
		Set myArray(i) = server.createobject("scripting.dictionary")
		
		For Each e In rs.Fields
			'rows = rows &""""& e.Name & """:""" & replace(e.value,chr(34),"@_'_@") & ""","
			myArray(i).Add e.Name,e.value  '将key/value加到行数组对象中
		Next 
		Rs.movenext
	Next
	
	total = rs.recordcount
	
elseif action = "about" then '单篇内容
	
	dim sortID
	
	sortID=getForm("sortid","get")

	sql = "select * from {prefix}Sort where SortID="&sortID
	
	set rs=conn.Exec(sql,"r1")
	Set jsonObj=New json
	jsonObj.toResponse=False
	Set json_ret = server.createobject("scripting.dictionary")
	
	Redim myArray(rs.recordcount - 1)
	
	
	For i=0 To rs.recordcount-1
		Set myArray(i) = server.createobject("scripting.dictionary")
		
		For Each e In rs.Fields
			'rows = rows &""""& e.Name & """:""" & replace(e.value,chr(34),"@_'_@") & ""","
			myArray(i).Add e.Name,e.value  '将key/value加到行数组对象中
		Next 
		Rs.movenext
	Next
	
	total = rs.recordcount
elseif action = "siteinfo" then '公司信息
	sql="select * from {prefix}Language where IsDefault=1"
	
	set rs=conn.Exec(sql,"r1")
	Set jsonObj=New json
	jsonObj.toResponse=False
	Set json_ret = server.createobject("scripting.dictionary")
	
	Redim myArray(rs.recordcount - 1)
	
	
	For i=0 To rs.recordcount-1
		Set myArray(i) = server.createobject("scripting.dictionary")
		
		For Each e In rs.Fields
			'rows = rows &""""& e.Name & """:""" & replace(e.value,chr(34),"@_'_@") & ""","
			myArray(i).Add e.Name,e.value  '将key/value加到行数组对象中
		Next 
		Rs.movenext
	Next
	
	total = rs.recordcount
elseif action = "slider" then '幻灯片
	dim slideid,img,imgs,txt,txts,slink,slinks
	slideid=getForm("slideid","get")

	if isnul(slideid) then slideid=1
	if not isnumeric(slideid) then slideid=1
	
	if slideid=1 then
		img=slideImgs
		txt=slideTexts
		slink=slideLinks
	elseif slideid=2 then
		img=slideImgsB
		txt=slideTextsB
		slink=slideLinksB
	elseif slideid=3 then
		img=slideImgsC
		txt=slideTextsC
		slink=slideLinksC
	elseif slideid=4 then
		img=slideImgsD
		txt=slideTextsD
		slink=slideLinksD
	end if
	if not isnul(img)  then
		imgs = split(img,",")
		txts = split(txt,",")
		slinks = split(slink,",")
	end if
	Redim myArray(ubound(imgs)-1)
	
	Set jsonObj=New json
	jsonObj.toResponse=False
	Set json_ret = server.createobject("scripting.dictionary")
	
	for i=0 to ubound(imgs)-1
		Set myArray(i) = server.createobject("scripting.dictionary")
		
		myArray(i).Add "img",Replace(imgs(i)," ","")
		myArray(i).Add "link",Replace(slinks(i)," ","")
		myArray(i).Add "title",Replace(txts(i)," ","")
		
	next
	
	total = ubound(imgs)
end if

	
json_ret.Add "total",total
json_ret.Add "list",myArray
jsonStr = jsonObj.toJSON(Empty,json_ret,False)

response.Write jsonStr

%>